﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LevelManager : MonoBehaviour {

    public GameManager GManager;

    public GameObject Circle;
    public GameObject CircleNB;

    private List<GameObject> circlePool;
    private List<GameObject> circleNBPool;
    private int poolSize = 24;
    private int poolSizeNB = 12;

    private int circleCount;
    private int circleNBCount;

    public List<GameObject> CirclePool
    {
        get { return circlePool; }
    }
    public List<GameObject> CircleNBPool
    {
        get { return circleNBPool; }
    }
    public int CircleCount
    {
        get { return circleCount; }
        set { circleCount = value; }
    }
    public int CircleNBCount
    {
        get { return circleNBCount; }
        set { circleNBCount = value; }
    }

    void Awake() {
        initPool();

        circleCount = 0;
        circleNBCount = -1;
    }
	
	void Update () {
		if (circleCount == circleNBCount && DOTween.TotalPlayingTweens() == 0)
        {
            circleCount = 0;
            circleNBCount = -1;

            GManager.FinishLevel();
        }
	}

    void initPool()
    {
        circlePool = new List<GameObject>();
        circleNBPool = new List<GameObject>();

        GameObject circles;
        GameObject circle;

        string parentGroup = GManager.ParentGroup;
        circles = new GameObject(parentGroup);

        for (int i = 0; i < poolSize; i++)
        {
            circle = (GameObject)Instantiate(Circle, new Vector3(0f, 0f, 0f), Quaternion.identity, circles.transform);
            circle.gameObject.SetActive(false);
            circlePool.Add(circle.gameObject);
        }

        for (int i = 0; i < poolSizeNB; i++)
        {
            circle = (GameObject)Instantiate(CircleNB, new Vector3(0f, 0f, 0f), Quaternion.identity, circles.transform);
            circle.gameObject.SetActive(false);
            circleNBPool.Add(circle.gameObject);
        }
    }

    GameObject addToPool(bool blendable)
    {
        GameObject circle;
        GameObject circles;

        string parentGroup = GManager.ParentGroup;
        circles = GameObject.Find(parentGroup);

        if (blendable)
        {
            circle = (GameObject)Instantiate(Circle, new Vector3(0f, 0f, 0f), Quaternion.identity, circles.transform);
            circle.gameObject.SetActive(false);
            circlePool.Add(circle.gameObject);
            poolSize++;
        }
        else
        {
            circle = (GameObject)Instantiate(CircleNB, new Vector3(0f, 0f, 0f), Quaternion.identity, circles.transform);
            circle.gameObject.SetActive(false);
            circleNBPool.Add(circle.gameObject);
            poolSizeNB++;
        }

        return circle;
    }

    public void LoadLevel(int levelNumber, float baseStartX, float baseStartY, float baseStep, int baseSize)
    {
        if (levelNumber < 0)
        {
            if (levelNumber == -1)
                GManager.showTutorial1();
            else
                GManager.showTutorial2();

            return;    
        }

        Level level;

        float startX = baseStartX;
        float startY = baseStartY;
        float step = baseStep;
        float offsetX, offsetY;

        if (GManager.Levels !=null)
        {
            level = GManager.Levels[levelNumber];

            offsetX = (level.SizeX % 2) * step / 2;
            offsetY = (level.SizeY % 2) * step / 2;

            startX += (baseSize - level.SizeX) / 2 * step + offsetX;
            startY += (baseSize - level.SizeY) / 2 * step + offsetY;

            circleCount = level.Circles.Count;
            circleNBCount = 0;
            
            GameObject circle = null;

            foreach (var circleDescription in level.Circles)
            {
                if (circleDescription.Blendable)
                {
                    for (int i = 0; i < circlePool.Count; i++)
                    {
                        if (!circlePool[i].activeInHierarchy && !DOTween.IsTweening(circlePool[i]))
                        {
                            circle = circlePool[i];
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < circleNBPool.Count; i++)
                    {
                        if (!circleNBPool[i].activeInHierarchy && !DOTween.IsTweening(circleNBPool[i]))
                        {
                            circle = circleNBPool[i];
                            break;
                        }
                    }
                }

                if (circle == null)
                    circle = addToPool(circleDescription.Blendable);

                circle.transform.position = new Vector3(startX + circleDescription.X * step, startY + circleDescription.Y * step, 0f);
                circle.GetComponent<SpriteRenderer>().color = circleDescription.CircleColor;
                circle.SetActive(true);
                circle.GetComponent<SpriteRenderer>().DOFade(1, 0.3f);

                if (!circleDescription.Blendable)
                    circleNBCount++;
            }
        }
        else
        {
            Debug.LogError("Levels list is empty");
        }
    }

    public void ClearLevel()
    {
        for (int i = 0; i < circlePool.Count; i++)
            if (circlePool[i].activeInHierarchy)
            {
                int temp = i;
                circlePool[temp].GetComponent<SpriteRenderer>().DOFade(0, 0.3f).OnComplete(() => circlePool[temp].SetActive(false));
            }

        for (int j = 0; j < circleNBPool.Count; j++)
            if (circleNBPool[j].activeInHierarchy)
            {
                int temp = j;
                circleNBPool[temp].GetComponent<SpriteRenderer>().DOFade(0, 0.3f).OnComplete(() => circleNBPool[temp].SetActive(false));
            }

        circleCount = 0;
        circleNBCount = -1;
    }

    public bool isFusable(GameObject startingCircle, GameObject endingCircle)
    {
        float xDif = System.Math.Abs(startingCircle.transform.position.x - endingCircle.transform.position.x);
        float yDif = System.Math.Abs(startingCircle.transform.position.y - endingCircle.transform.position.y);

        if (xDif + yDif == GManager.Step)
        {
            if (startingCircle.gameObject.name == "circleNB(Clone)" || endingCircle.name == "circleNB(Clone)")
                if (startingCircle.GetComponent<SpriteRenderer>().color == endingCircle.GetComponent<SpriteRenderer>().color)
                    return true;
                else
                    return false;
            else
                return true;
        }
        return false;
    }

    public void Fuse(GameObject startingCircle, GameObject endingCircle)
    {

        if (startingCircle.gameObject.name == "circleNB(Clone)")
        {
            startingCircle.transform.DOMove(endingCircle.transform.position, 0.3f).OnComplete(() => endingCircle.SetActive(false));

            if (startingCircle.gameObject.name == "circleNB(Clone)" && endingCircle.gameObject.name == "circleNB(Clone)")
                circleNBCount--;
        }
        else
        {
            Color32 startingColor = startingCircle.GetComponent<SpriteRenderer>().color;
            Color32 endingColor = endingCircle.GetComponent<SpriteRenderer>().color;
            Color32 fusedColor = Color32.Lerp(startingColor, endingColor, 0.5f);
            endingCircle.GetComponent<SpriteRenderer>().DOColor(fusedColor, 0.3f);
            startingCircle.GetComponent<SpriteRenderer>().DOColor(fusedColor, 0.3f);
            startingCircle.transform.DOMove(endingCircle.transform.position, 0.3f).OnComplete(() => startingCircle.SetActive(false));
            //Debug.Log(fusedColor);
        }
        
        circleCount--;
    }
}
