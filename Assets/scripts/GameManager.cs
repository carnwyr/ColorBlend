﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.scripts;
using DG.Tweening;

public enum State { Menu, Levels, Playing, Finished };

public class GameManager : MonoBehaviour
{
    private string levelDataFileName = "levels";
    private const string parentGroup = "Circles";

    private float baseStartX = -2.5f;
    private float baseStartY = -2.5f;
    private float baseStep = 1f;

    private List<Level> levels;

    private LevelManager LManager;

    private State gameState;
    private int currentLevel;

    private GameObject mainMenu;
    private GameObject playMenu;
    private GameObject levelMenu;
    private GameObject content;
    private GameObject levelFinished;
    private GameObject nextLevel;
    private GameObject noMoreLevels;
    private GameObject instruction;
    private GameObject resultingColor;

    private const int baseSize = 6;

    public float Step
    {
        get { return baseStep; }
    }
    public string ParentGroup
    {
        get { return parentGroup; }
    }
    public List<Level> Levels
    {
        get { return levels; }
    }
    public State GameState
    {
        get { return gameState; }
    }

    public GameObject LevelButton;

    void Awake()
    {
        LManager = gameObject.GetComponent<LevelManager>();
        mainMenu = GameObject.Find("MainMenu");
        playMenu = GameObject.Find("Playing");
        levelMenu = GameObject.Find("LevelSelect");
        content = GameObject.Find("Content");
        levelFinished = GameObject.Find("LevelFinished");
        nextLevel = GameObject.Find("NextLevel");
        noMoreLevels = GameObject.Find("NoLevels");
        instruction = GameObject.Find("Instruction");
        resultingColor = GameObject.Find("ResultingColor");
    }

    void Start()
    {
        //PlayerPrefs.DeleteAll();

        levels = new List<Level>();
        try
        {
            levels = loadLevels();
        }
        catch(System.Exception e)
        {
            //should work for both exceptions
            Debug.LogError(e.Message);
            Application.Quit();
        }

        addButtons();

        playMenu.transform.DOLocalMoveY(100, 0.5f);
        playMenu.SetActive(false);
        levelMenu.SetActive(false);
        nextLevel.SetActive(false);
        noMoreLevels.SetActive(false);
        levelFinished.SetActive(false);
        instruction.SetActive(false);
        resultingColor.SetActive(false);

        gameState = State.Menu;
    }

    void Update()
    {

    }

    List<Level> loadLevels()
    {
        var dataFile = Resources.Load(levelDataFileName) as TextAsset;
        LevelsWrapper loadedData = new LevelsWrapper();

        if (dataFile != null)
        {
            loadedData = JsonUtility.FromJson<LevelsWrapper>(dataFile.text);
            if (loadedData == null || loadedData.levels == null)
            {
                //this shouldn't be possible, should it?
                throw new System.Exception("No levels!");
            }
        }
        else
        {
            throw new System.IO.FileNotFoundException("levels.json is missing!");
        }

        return loadedData.levels;
    }

    public void ChangeState(State state)
    {
        switch(state)
        {
            case State.Menu:
                break;
            case State.Levels:
                if(mainMenu.activeInHierarchy)
                    mainMenu.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() => mainMenu.SetActive(false));
                if (playMenu.activeInHierarchy)
                    playMenu.transform.DOLocalMoveY(100, 0.5f).OnComplete(() => playMenu.SetActive(false));
                if(instruction.activeInHierarchy)
                    instruction.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() => instruction.SetActive(false));
                if (levelFinished.activeInHierarchy)
                    levelFinished.transform.DOLocalMoveY(800, 1).OnComplete(() => levelFinished.SetActive(false));
                levelMenu.SetActive(true);
                levelMenu.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
                makeButtonsInteractable();
                break;
            case State.Playing:
                if(levelMenu.activeInHierarchy)
                    levelMenu.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() => levelMenu.SetActive(false));
                if (levelFinished.activeInHierarchy)
                    levelFinished.transform.DOLocalMoveY(800, 1).OnComplete(() => levelFinished.SetActive(false));
                playMenu.SetActive(true);
                playMenu.transform.DOLocalMoveY(0, 0.5f);
                break;
            case State.Finished:
                if (instruction.activeInHierarchy)
                    instruction.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() => instruction.SetActive(false));
                levelFinished.SetActive(true);
                if (currentLevel == levels.Count - 1)
                {
                    noMoreLevels.SetActive(true);
                    nextLevel.SetActive(false);
                }
                else
                {
                    nextLevel.SetActive(true);
                    noMoreLevels.SetActive(false);
                }
                levelFinished.transform.DOLocalMoveY(100, 1);
                break;
        }

        gameState = state;
    }

    void addButtons()
    {
        GameObject button;

        button = (GameObject)Instantiate(LevelButton, content.transform);
        button.name = "Tutorial";
        button.GetComponentInChildren<Text>().text = "Tutorial";
        button.GetComponent<Button>().onClick.AddListener(() => { showTutorial1(); });
        button.GetComponent<Button>().interactable = true;
        button.GetComponentInChildren<Text>().color = new Color32(50, 50, 50, 255);

        for (int i = 0; i < levels.Count; i++)
        {
            var temp = i;
            button = (GameObject)Instantiate(LevelButton, content.transform);
            button.name = "Level " + (i + 1).ToString();
            button.GetComponentInChildren<Text>().text = "Level " + (i + 1).ToString();
            button.GetComponent<Button>().onClick.AddListener( () => { startLevel(temp); } );
        }
    }

    void makeButtonsInteractable()
    {
        for (int i = 0; i < PlayerPrefs.GetInt("LevelsOpen", 0); i++)
        {
            GameObject button = GameObject.Find("Level " + (i + 1).ToString());
            if (!button.GetComponent<Button>().IsInteractable())
            {
                button.GetComponent<Button>().interactable = true;
                button.GetComponentInChildren<Text>().color = new Color32(50, 50, 50, 255);
            }
        }
    }

    void startLevel(int level)
    {
        ChangeState(State.Playing);
        currentLevel = level;
        LManager.LoadLevel(currentLevel, baseStartX, baseStartY, baseStep, baseSize);
    }

    public void RestartLevel()
    {
        if (DOTween.TotalPlayingTweens() > 0)
            return;

        if (gameState != State.Playing)
        {
            ChangeState(State.Playing);
        }
        LManager.ClearLevel();
        LManager.LoadLevel(currentLevel, baseStartX, baseStartY, baseStep, baseSize);
    }

    public void OpenMenu()
    {
        LManager.ClearLevel();
        ChangeState(State.Levels);
    }

    public void FinishLevel()
    {
        if (currentLevel == -1)
        {
            LManager.ClearLevel();
            showTutorial2();
        }
        else
        {
            if (currentLevel == PlayerPrefs.GetInt("LevelsOpen", 0))
                PlayerPrefs.SetInt("LevelsOpen", currentLevel + 1);
            LManager.ClearLevel();
            //probably should fix this
            if (currentLevel == -2)
                currentLevel = -1;
            ChangeState(State.Finished);
        }
    }

    public void NextLevel()
    {
        int newLevel = currentLevel + 1;
        //LManager.ClearLevel();
        startLevel(newLevel);
    }

    private GameObject getFreeCircle()
    {
        for (int i = 0; i < LManager.CirclePool.Count; i++)
            if (!LManager.CirclePool[i].activeInHierarchy && !DOTween.IsTweening(LManager.CirclePool[i]))
                return LManager.CirclePool[i];

        return LManager.CirclePool[0];
    }

    private GameObject getFreeCircleNB()
    {
        for (int i = 0; i < LManager.CircleNBPool.Count; i++)
            if (!LManager.CircleNBPool[i].activeInHierarchy && !DOTween.IsTweening(LManager.CircleNBPool[i]))
                return LManager.CircleNBPool[i];

        return LManager.CircleNBPool[0];
    }

    public void showTutorial1()
    {
        instruction.SetActive(true);
        instruction.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
        instruction.GetComponent<Text>().text = "You can fuse two vertically or horizontally adjacent circles. Swipe from one circle to another to preview the resulting color between the Menu and Restart buttons, release to fuse.";
        
        GameObject circle = getFreeCircle();
        float posX = baseStartX + (baseSize - 2) / 2 * baseStep;
        float posY = baseStartY + (baseSize - 1) / 2 * baseStep + baseStep / 2;
        circle.transform.position = new Vector3(posX , posY, 0f);
        circle.GetComponent<SpriteRenderer>().color = new Color32(130, 230, 200, 0);
        circle.SetActive(true);
        circle.GetComponent<SpriteRenderer>().DOFade(1, 0.3f);

        circle = getFreeCircle();
        posX = baseStartX + (baseSize - 2) / 2 * baseStep + baseStep;
        circle.transform.position = new Vector3(posX, posY, 0f);
        circle.GetComponent<SpriteRenderer>().color = new Color32(230, 160, 130, 0);
        circle.SetActive(true);
        circle.GetComponent<SpriteRenderer>().DOFade(1, 0.3f);

        currentLevel = -1;
        LManager.CircleCount = 2;
        LManager.CircleNBCount = 1;
        ChangeState(State.Playing);
    }

    public void showTutorial2()
    {

        instruction.GetComponent<Text>().text = "Rings must be fused with circles of the same color. The level is finished when only the rings are left.";
        GameObject circle = getFreeCircle();
        float posX = baseStartX + (baseSize - 2) / 2 * baseStep;
        float posY = baseStartY + (baseSize - 1) / 2 * baseStep + baseStep / 2;
        circle.transform.position = new Vector3(posX, posY, 0f);
        circle.GetComponent<SpriteRenderer>().color = new Color32(140, 200, 170, 0);
        circle.SetActive(true);
        circle.GetComponent<SpriteRenderer>().DOFade(1, 0.3f);

        circle = getFreeCircle();
        posY = baseStartY + (baseSize - 1) / 2 * baseStep + baseStep / 2 - baseStep;
        circle.transform.position = new Vector3(posX, posY, 0f);
        circle.GetComponent<SpriteRenderer>().color = new Color32(160, 180, 150, 0);
        circle.SetActive(true);
        circle.GetComponent<SpriteRenderer>().DOFade(1, 0.3f);

        circle = getFreeCircleNB();
        posX = baseStartX + (baseSize - 2) / 2 * baseStep + baseStep;
        circle.transform.position = new Vector3(posX, posY, 0f);
        circle.GetComponent<SpriteRenderer>().color = new Color32(150, 190, 160, 0);
        circle.SetActive(true);
        circle.GetComponent<SpriteRenderer>().DOFade(1, 0.3f);

        currentLevel = -2;
        LManager.CircleCount = 3;
        LManager.CircleNBCount = 1;
    }

    public void ShowResultingColor(GameObject startingCircle, GameObject endingCircle)
    {
        Color32 startingColor = startingCircle.GetComponent<SpriteRenderer>().color;
        Color32 endingColor = endingCircle.GetComponent<SpriteRenderer>().color;
        Color32 fusedColor = Color32.Lerp(startingColor, endingColor, 0.5f);
        if (!resultingColor.activeInHierarchy)
        {
            fusedColor.a = 0;
            resultingColor.GetComponent<SpriteRenderer>().color = fusedColor;
            resultingColor.SetActive(true);
            resultingColor.GetComponent<SpriteRenderer>().DOFade(1, 0.3f);
        }
        else
        {
            resultingColor.GetComponent<SpriteRenderer>().DOColor(fusedColor, 0.3f);
        }
    }

    public void HideResultingColor()
    {
        resultingColor.GetComponent<SpriteRenderer>().DOFade(0, 0.3f).OnComplete(() => resultingColor.SetActive(false));
    }
}
