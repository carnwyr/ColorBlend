﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.scripts
{
    [Serializable]
    public class LevelsWrapper
    {
        public List<Level> levels;
    }

    [Serializable]
    public class Level
    {
        [SerializeField]
        string name;
        [SerializeField]
        int sizeX;
        [SerializeField]
        int sizeY;
        [SerializeField]
        List<CircleDescription> circles;

        public string Name
        {
            get { return name; }
        }

        public int SizeX
        {
            get { return sizeX; }
        }

        public int SizeY
        {
            get { return sizeY; }
        }

        public List<CircleDescription> Circles
        {
            get { return circles; }
        }
    }

    [Serializable]
    public class CircleDescription
    {
        //in the name of encapsulation!
        [SerializeField]
        int x;
        [SerializeField]
        int y;
        [SerializeField]
        byte r;
        [SerializeField]
        byte g;
        [SerializeField]
        byte b;
        [SerializeField]
        bool blendable;
        
        public int X
        {
            get { return x; }
        }
        
        public int Y
        {
            get { return y; }
        }
        
        public Color32 CircleColor
        {
            get { return new Color32(r, g, b, 0);  }
        }

        public bool Blendable
        {
            get { return blendable; }
        }
    }
}
