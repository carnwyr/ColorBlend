﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using DG.Tweening;

public class InputManager : MonoBehaviour {

    public GameManager GManager;
    public LevelManager LManager;

    private GameObject startingCircle;
    private GameObject endingCircle;

    private Vector2 pos_start;
    private Vector2 pos_moved;

    
    void Start () {
		
	}
	
	void Update () {
        if (Input.touchCount > 0)
        {
            switch (GManager.GameState)
            {
                case State.Menu:
                    GManager.ChangeState(State.Levels);
                    break;
                case State.Levels:
                    break;
                case State.Playing:
                    switch (Input.GetTouch(0).phase)
                    {
                        case TouchPhase.Began:
                            OnTouchStart();
                            break;
                        case TouchPhase.Moved:
                            OnTouchMove();
                            break;
                        case TouchPhase.Ended:
                            OnTouchEnd();
                            break;
                    }
                    break;
            }
        }
    }

    void OnTouchStart()
    {
        pos_start = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        pos_moved = pos_start;

        RaycastHit2D raycastHit;
        raycastHit = Physics2D.Raycast(pos_start, Vector2.zero);
        if (raycastHit.collider != null)
        {
            if (raycastHit.transform.parent.name == GManager.ParentGroup && !DOTween.IsTweening(raycastHit.transform))
            {
                //stretch starting
                startingCircle = raycastHit.transform.gameObject;
                startingCircle.transform.DOScale(0.25f, 0.4f);
            }
        }
    }

    void OnTouchMove()
    {
        if (startingCircle == null)
            return;

        pos_moved = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

        var distance = pos_moved - pos_start;

        if (Math.Abs(distance[0]) < GManager.Step/2 && Math.Abs(distance[1]) < GManager.Step/2)
            return;

        Vector2 direction;

        if (distance[1] >= distance[0])
            if (distance[1] >= -distance[0])
                direction = Vector2.up;
            else
                direction = Vector2.left;
        else if (distance[1] <= -distance[0])
            direction = Vector2.down;
        else
            direction = Vector2.right;

        Vector2 pos_target = pos_start + direction * GManager.Step;

        RaycastHit2D raycastHit;
        raycastHit = Physics2D.Raycast(pos_target, Vector2.zero);
        if (raycastHit.collider != null)
        {
            if ((raycastHit.transform.parent.name == GManager.ParentGroup) && (raycastHit.transform.gameObject != startingCircle))
            {
                if (endingCircle == null)
                {
                    if (LManager.isFusable(startingCircle, raycastHit.transform.gameObject))
                    {
                        //stretch ending
                        endingCircle = raycastHit.transform.gameObject;
                        endingCircle.transform.DOScale(0.25f, 0.4f);
                        GManager.ShowResultingColor(startingCircle, endingCircle);
                    }
                }
                else
                {
                    if (LManager.isFusable(startingCircle, raycastHit.transform.gameObject))
                    {
                        if (raycastHit.transform.gameObject != endingCircle)
                        {
                            //shrink old
                            //stretch new
                            endingCircle.transform.DOScale(0.2f, 0.4f);
                            endingCircle = raycastHit.transform.gameObject;
                            endingCircle.transform.DOScale(0.25f, 0.4f);
                            GManager.ShowResultingColor(startingCircle, endingCircle);
                        }
                    }
                    else
                    {
                        //shrink old
                        endingCircle.transform.DOScale(0.2f, 0.4f);
                        endingCircle = null;
                        GManager.HideResultingColor();
                    }
                }
            }
            else if (endingCircle != null)
            {
                //shrink old
                endingCircle.transform.DOScale(0.2f, 0.4f);
                endingCircle = null;
                GManager.HideResultingColor();
            }
        }
        else if (endingCircle != null)
        {
            //shrink old
            endingCircle.transform.DOScale(0.2f, 0.4f);
            endingCircle = null;
            GManager.HideResultingColor();
        }
    }

    void OnTouchEnd()
    {
        if (startingCircle != null && endingCircle != null)
        {
            LManager.Fuse(startingCircle, endingCircle);
            //Debug.Log("Fused circles");
        }
        if (startingCircle != null)
        {
            //starting circle shrink
            startingCircle.transform.DOScale(0.2f, 0.4f);
            startingCircle = null;
        }
        if (endingCircle != null)
        {
            //ending circle shrink
            endingCircle.transform.DOScale(0.2f, 0.4f);
            endingCircle = null;
            GManager.HideResultingColor();
        }
    }
}
